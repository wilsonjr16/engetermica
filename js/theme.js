jQuery(document).ready( function(){
		
	jQuery('.hamburguer').click(function(){
	jQuery('.mobile_header, .nav_mobile_wrapper').addClass('active');
	jQuery('.blackout2').fadeIn();
	jQuery('body').addClass('scroll-hidden');
	});

	jQuery('.menu_close').click(function(){
		jQuery('.mobile_header, .nav_mobile_wrapper').removeClass('active');
		jQuery('.blackout2').fadeOut();
		jQuery('body').removeClass('scroll-hidden');
	});

	var html = $('html');

	// triggers to open and close menu mobile
	$('.trigger-menu, .menu-mobile-backdrop').click(function(){
	   html.addClass('menu-open'); 
	});

	$('.menu-mobile-backdrop,.backdrop-icon').click(function(){
	   html.removeClass('menu-open'); 
	});

	// cart support class
	if($('.caixa-cupom').length){
		$('.caixa-cupom').parents('tr').addClass('cupom-wrapper');
	}

	if($('#calculoFrete').length){
		$('#calculoFrete').parents('tr').addClass('frete-wrapper');
	}
              
    
    });

jQuery(document).ready(function(){
     Formzin.iniciar();
});


jQuery(document).ready(function(){
  function call_ajax_withCallback(_url, _data, callback){

    jQuery.ajax({
        type: "POST",
        url: _url,
        data: _data,
        async: true,
        success: callback,
      error: callback
    });

  } /* call_ajax_withCallback */



jQuery('.btn_form').click(function(e){
        // e.preventDefault();
        console.log("Clicou no botão");

        var name    = $('#nome');
        var mail    = $('#email');
        var fone    = $('#telefone');
        var assunto    = $('#assunto');
        var msg     = $('#mensagem');

        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        if( $.trim( name.val() ) == "" ){

            name.addClass('error');
            $('.error_text').text('Preencha seu nome!').slideDown();

            return false;

        }

        if ( !emailReg.test( $.trim( mail.val() ) ) || $.trim( mail.val() ) == "" ){

            mail.addClass('error');
            $('.error_text').text('Preencha seu email corretamente!').slideDown();

            return false;

        }

        if( $.trim( msg.val() ) == "" ){

            msg.addClass('error');
            $('.error_text').text('Preencha sua mensagem!').slideDown();

            return false;

        }


        var User = {};

        User.name    = name.val();
        User.mail    = mail.val();
        User.fone   = fone.val();
        User.assunto    = assunto.val();
        User.mensagem = msg.val();

        $('.spinner').show();

        call_ajax_withCallback('./mail.php', {'User' : User}, function(data){

            $('.spinner').hide();

            if( data.indexOf('sucesso') > -1 ){

                $('.sucess_text').slideDown().text('Parabéns! Seu e-mail foi enviado com sucesso.');

            }else {

                $('.error_text').slideDown().text('Ocorreu um erro. Tente novamente.');

            }

        });


        return false;

    });
  });
