<!DOCTYPE html>

<html lang="pt-br">
<head>
    <title><?php print $cabecalho_title; ?></title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/bootstrap.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

	<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="js/formzin-1.0.4.min.js" type="text/javascript"></script>
	<script src="js/theme.js" type="text/javascript"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body class="<?php print $classe; ?>">
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9&appId=480535522147553";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
	<header class="header_main fleft_full">
		<div class="header_sup cem">
			<div class="center_element">

				<div class="header_sup-box">
					<div class="header_contato">
						<span class="header_fone">FONE: 85 3022-8080</span>
					</div>

					<div class="header_info">
						<ul class="header_info-list">
							<li class="header_info-item"><a class="header_info-link" href="parceiros.php">PARCEIROS</a></li>
							<li class="header_info-item"><a class="header_info-link" href="representadas.php">REPRESENTADAS</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>


		<section class="header_section-navigation cem">
			<div class="center_element">
				<nav class="header_nav">

					<h1 class="header_logo">
						<a href="index.php" class="header_link">
							<img src="img/logo.png" alt="imagem da logo - home" class="header_logo-img center-block img-responsive">
						</a>
					</h1>

					<div class="header_block">
						<ul class="header_menu">
							<li class="header_menu-item">
								<a href="index.php" class="header_menu-link">HOME</a>
							</li>

							<li class="header_menu-item">
								<a href="sobre.php" class="header_menu-link">SOBRE</a>
							</li>

							<li class="header_menu-item">
								<a href="servicos.php" class="header_menu-link">SERVIÇOS</a>
							</li>

							<li class="header_menu-item">
								<a href="produtos.php" class="header_menu-link">PRODUTOS</a>
							</li>

							<li class="header_menu-item">
								<a href="contato.php" class="header_menu-link">CONTATO</a>
							</li>
						</ul>
					</div>

				</nav>
			</div>
		</section>
	</header>

	<header class="header_mobile fleft_full">

		<div class="hamburguer">
			<div class="hamburguer_item first"></div>
			<div class="hamburguer_item middle"></div>
			<div class="hamburguer_item last"></div>
		</div>

		<h1 class="logo_mobile">
			<a class="logo_mobile-link" href="/">
				<img src="img/logo.png" alt="imagem da logo" class="img_logo-mobile img-responsive"/>
			</a>
		</h1>

		<div class="nav_mobile_wrapper">

			<div class="nav_mobile_col fleft">

				<h1 class="logo_mobile">
					<a class="logo_mobile-link" href="/">
						<img src="img/logo.png" alt="imagem da logo" class="img_logo-mobile img-responsive"/>
					</a>
				</h1>

				<div class="menu_close"></div>
				
				<div class="borda-divisao"></div>

				<nav class="header_mobile-nav">

						<ul class="header_menu">
							<li class="header_menu-item">
								<a href="index.php" class="header_menu-link">HOME</a>
							</li>

							<li class="header_menu-item">
								<a href="sobre.php" class="header_menu-link">SOBRE</a>
							</li>

							<li class="header_menu-item">
								<a href="servicos.php" class="header_menu-link">SERVIÇOS</a>
							</li>

							<li class="header_menu-item">
								<a href="produtos.php" class="header_menu-link">PRODUTOS</a>
							</li>

							<li class="header_menu-item">
								<a href="contato.php" class="header_menu-link">CONTATO</a>
							</li>
						</ul>

				</nav>

			</div>

		</div>

	</header>
