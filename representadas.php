<?php
	$cabecalho_title = "Representadas";
	include("header.php");
?>

<section class="section_parceiros fleft_full">
	<div class="center_element">
		<div class="section_parceiros-box">
			<div class="col-md-12">
				<div class="inf_parceiros">
					<h2 class="tit_parceiros">Representadas</h2>

					<ul class="list_parceiros row">
						<li class="item_parceiro col-md-3">
							<img src="img/representadas/representada1.jpg" class="img-responsive center-block">
							<h3 class="name_parceiro">Alfalaval</h3>
							<p class="desc_parceiro">Petrópolis - Rio de Janeiro - Brasil</p>
							<a href="link_parceiro" title="Site do parceiro" class="link_parceiro">http://www.alfalaval.com/</a>
						</li>

						<li class="item_parceiro col-md-3">
							<img src="img/representadas/representada2.jpg" class="img-responsive center-block">
							<h3 class="name_parceiro">Netzsch</h3>
							<p class="desc_parceiro">Pomerode - Santa Catarina - Brasil</p>
							<a href="link_parceiro" title="Site do parceiro" class="link_parceiro">https://www.netzsch.com.br</a>
						</li>

						<li class="item_parceiro col-md-3">
							<img src="img/representadas/representada3.jpg" class="img-responsive center-block">
							<h3 class="name_parceiro">Grundfos Brasil </h3>
							<p class="desc_parceiro">São Bernardo do Campo – São Paulo – Brasil</p>
							<a href="link_parceiro" title="Site do parceiro" class="link_parceiro">http://www.grundfos.com.br</a>
						</li>

						<li class="item_parceiro col-md-3">
							<img src="img/representadas/representada4.jpg" class="img-responsive center-block">
							<h3 class="name_parceiro">Nord DriveSystems </h3>
							<p class="desc_parceiro">Guarulhos - São Paulo – Brasil</p>
							<a href="link_parceiro" title="Site do parceiro" class="link_parceiro">https://www.nord.com</a>
						</li>

					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	include("footer.php");
?>
