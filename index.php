<?php
	$classe ="page-home";
	$cabecalho_title = "Engetermica";
	include("header.php");
?>
	<div class="shape">
		<!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1192px" height="1000px"><path fill-rule="evenodd"  fill="#003065" d="M481.734,450.679 C217.349,60.639 0.019,0.461 0.019,0.461 L1132.000,1.000 C1165.858,1.000 1191.000,1.000 1191.000,1.000 L1191.982,998.965 C983.612,1003.422 746.117,840.719 481.734,450.679 Z"/></svg> -->
		<img src="img/shape.png" class="img-responsive">
	</div>
	<div class="clear"></div>

	<section class="section_lv1 fleft_full">

		<div class="center_element">

			<div class="section_lv1-box col-xs-12">

        		<div class="col-md-6 col-xs-12 cald">

					<figure class="cald_box">
						<img src="img/caldeira.png" alt="imagem ilustrativa de uma caldeira" class="cald_box-img img-responsive">
					</figure>

				</div>

				<div class="col-md-6 cal-xs-12 cald_inf">
					<h2 class="cald_inf-tit">CALDEIRA DE VAPOR</h2>
					<p class="cald_inf-txt">Caldeiras a óleo / caldeiras a gás</p>
					<p class="cald_inf-txt">Modelo MISSIONTM3-PASS 2,0 - 34 t/h</p>
					<p class="cald_inf-txt">Modelo ATA 0,33 - 2 t/h</p>
					<p class="cald_inf-txt">Modelo CV 0,25 - 0,65 t/h</p>
					<p class="cald_inf-txt">Caldeiras a biomassa (lenha, cavaco, briquete,<br>pellet, bagaço) / Caldeira a óleo</p>
					<p class="cald_inf-txt">Modelo FAM 10 - 15 - 20 t/h</p>
					<p class="cald_inf-txt">Modelo LHC 1 - 6,5 t/h</p>
					<p class="cald_inf-btn"><a class="cald_inf-link" href="#">ver mais</a></p>
				</div>

			</div><!-- section_lv1-box -->



			<div class="clear"></div>

		</div><!-- center element -->

	</section><!-- section_lv1 -->

	<div class="clear"></div>

	<section class="section_lv2 fleft_full">

		<div class="center_element">

			<div class="section_lv2-box">

				<div class="sobre_box-local col-md-6 col-xs-12">
					<figure class="sobre_box-img">
						<img src="img/fachada.png" alt="imagem da fachada" class="sobre_local-img img-responsive">
					</figure>
					<p class="sobre_local-txt">Constituída em julho de 1991, em Recife (PE), voltada inicialmente para a execução de serviços técnicos em geração e distribuição de vapor.</p>
				</div>

				<div class="sobre_box-nav col-md-5 col-md-offset-1 col-xs-12">
					<ul class="sobre_box-menu">
						<li class="sobre_menu-item"><a class="sobre_menu-link" href="sobre.php#missao">Missão</a></li>
						<li class="sobre_menu-item"><a class="sobre_menu-link" href="sobre.php#visao">Visão</a></li>
					</ul>
				</div>

			</div><!-- section lv2 box -->

		</div><!-- center element -->

	</section><!-- section_lv2 -->

	<div class="clear"></div>

	<section class="section_lv3 fleft_full">
		<div class="center_element">
			<h2 class="section_lv3-tit">PRODUTOS</h2>

			<ul class="prat">
				<li class="prat_item col-md-3">
					<a class="prat_link" href="produto.php" title="Bomba NEMO BG">
						<img src="img/prod1.jpg" alt="imagem do produto" class="img-responsive prat_img">
						<div class="btn_comprar">Bomba NEMO BG</div>
					</a>
				</li>

				<li class="prat_item col-md-3">
					<a class="prat_link" href="produto.php" title="Aalborg Mini 3-Pass">
						<img src="img/prod2.jpg" alt="imagem do produto" class="img-responsive prat_img">
						<div class="btn_comprar">Aalborg Mini 3-Pass</div>
					</a>
				</li>

				<li class="prat_item col-md-3">
					<a class="prat_link" href="produto.php" title="Nordblock .1">
						<img src="img/prod3.jpg" alt="imagem do produto" class="img-responsive prat_img">
						<div class="btn_comprar">Nordblock .1</div>
					</a>
				</li>

				<li class="prat_item col-md-3">
					<a class="prat_link" href="produto.php" title="Bombas de pistão com módulo de pressurização">
						<img src="img/prod4.jpg" alt="imagem do produto" class="img-responsive prat_img">
						<div class="btn_comprar">Bombas de pistão com módulo de pressurização</div>
					</a>
				</li>
			</ul>
		</div>
	</section>

	<div class="clear"></div>

	<?php
		include("footer.php");
	?>
