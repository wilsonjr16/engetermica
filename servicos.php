<?php
	$cabecalho_title = "Serviços";
	include("header.php");
?>

<section class="section_product fleft_full">
	<div class="center_element">
		<div class="section_product-box">
			<div class="col-md-6">
				<div class="inf_product">
					<h2 class="tit_product">Serviços</h2>
					<p class="desc_service">Confira alguns dos serviços que prestamos:</p>
					<ol class="lista-servicos">
						<li>Serviços de instalação</li>
						<li>Montagem e manutenção de caldeiras</li>
						<li>Geradores de vapor  e equipamentos industriais com mão de obra qualificada</li>
						<li>Serviços de regulagem da combustão de gerador</li>
						<li>Diminuição dos gases poluentes e economia de combustível</li>
						<li>Treinamentos para operadores de caldeira com certificado (dividido em duas partes teórica e estágio prático supervisionado no equipamento de trabalho)</li>
						<li>Inspeção de segurança</li>
						<li>Manutenção de Bomba e Motoredutores</li>
					</ol>
				</div>
			</div>

			<div class="col-md-5 col-md-offset-1 servicos_img-box">
				<figure>
					<img src="img/servicos.jpg" alt="imagem representativa" class="img-responsive">
				</figure>
			</div>
		</div>
	</div>
</section>


<?php
	include("footer.php");
?>
