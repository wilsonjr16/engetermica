<?php
	$cabecalho_title = "Contato";
	include("header.php");
?>

<section class="contato fleft_full">
	<div class="center_element">
		<div class="col-md-6">
			<h3 class="contato_tit col-md-12">Fale conosco</h3>
			<p class="contato_txt">Preencha todos os campos para que possamos entrar em contato com você.
				A Engetérmica, agradece.
			</p>
			<form id="contato" class="form_contact">
				<p class="contato_campos">
					<label  for="nome">Nome:</label>
					<input type="text" name="nome" placeholder="Digite aqui o seu nome" id="nome" required="required" >
				</p>

				<p class="contato_campos">
					<label for="email">Email:</label>
					<input type="email" name="email" placeholder="exemplo@email.com" id="email" required="required">
				</p>

				<p class="contato_campos">
					<label for="telefone">Telefone:</label>
					<input type="text" name="telefone" class="telefone" placeholder="(00) 0000-0000" id="telefone" required="required">
				</p>

				<p class="contato_campos">
					<label for="assunto">Assunto:</label>
					<input type="text" name="assunto" placeholder="Digite aqui o assunto" id="assunto" required="required">
				</p>

				<p class="contato_campos">
					<label for="msg">Mensagem:</label>
					<textarea  name="mensagem" placeholder="Deixe aqui sua mensagem" id="mensagem" required="required"></textarea>
				</p>

				<p class="error_text">asdasd</p>

        <p class="sucess_text">asdasd</p>

				<input type="submit" class="btn_form" value="ENVIAR">

			</form>
		</div>

		<div class="col-md-5 col-md-offset-1">
			<div class="localizacao">
				<h3 class="localizacao_tit">Onde Estamos</h3>
				<div class="box_map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.1932245714816!2d-38.54843618582447!3d-3.7680902972586754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x670da5c5dfb3fd5f!2sEnget%C3%A9rmica+Servi%C3%A7os+T%C3%A9rmicos+Com+e+Rep!5e0!3m2!1spt-BR!2sbr!4v1500037280499" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="localizazao_infos">
					<ul class="localizacao_list">
						<li class="fale_item">
							<p class="fale_txt">Rua Raimundo Edmundo Linhares, 256 - Montese.</p>
							<p class="fale_txt">CEP: 60420-340 / Fortaleza - Ceará.</p>
						</li>

						<li class="fale_item">
							<p class="fale_txt">Fone: +55 85 3022-8080</p>
						</li>

						<li class="fale_item">
							<a class="fale_email-link" href="#">contato@engetermica.com.br</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

	</div>
</section>

<?php
	include("footer.php");
?>
