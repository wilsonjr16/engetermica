<?php
	$cabecalho_title = "Produtos";
	include("header.php");
?>

<section class="section_product fleft_full">
	<div class="center_element">
		<div class="section_product-box">
			<div class="col-md-6">
				<div class="inf_product">
					<h2 class="tit_product">Produtos</h2>
					<p class="desc_product">Comercialização de caldeiras, geradores de vapor, tanques, vasos de pressão em geral e seus componentes, bombas, válvulas, tubos, conexões,
						material elétrico, material refratário, material isolante, material para solda, pressostatos, detectores de chama, medidores de vazão, peças e acessórios para
						vapor.
					</p>

					<p class="desc_product">Confira alguns de nossos produtos: </p>
				</div>
			</div>

			<div class="col-md-5 col-md-offset-1 produtos_img-box">
				<figure>
					<img src="img/caldeira.png" alt="imagem representativa" class="img-responsive">
				</figure>
			</div>
		</div>
	</div>
</section>

<section class="section_lv3 fleft_full">
	<div class="center_element">

		<ul class="prat">
			<li class="prat_item col-md-3">
				<div class="prat_link">
					<img src="img/prod1.jpg" alt="imagem do produto" class="img-responsive prat_img">
					<div class="btn_comprar">Bomba NEMO BG</div>
				</div>
			</li>

			<li class="prat_item col-md-3">
				<div class="prat_link">
					<img src="img/prod2.jpg" alt="imagem do produto" class="img-responsive prat_img">
					<div class="btn_comprar">Aalborg Mini 3-Pass</div>
				</div>
			</li>

			<li class="prat_item col-md-3">
				<div class="prat_link">
					<img src="img/prod3.jpg" alt="imagem do produto" class="img-responsive prat_img">
					<div class="btn_comprar">Nordblock .1</div>
				</div>
			</li>

			<li class="prat_item col-md-3">
				<div class="prat_link">
					<img src="img/prod4.jpg" alt="imagem do produto" class="img-responsive prat_img">
					<div class="btn_comprar">Bombas de pistão com módulo de pressurização</div>
				</div>
			</li>
		</ul>
	</div>
</section>

<?php
	include("footer.php");
?>
