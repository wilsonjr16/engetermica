<?php
	$cabecalho_title = "Sobre";
	include("header.php");
?>

<section class="section_sobre fleft_full">
	<div class="center_element">
		<div class="section_sobre-box">
			<div class="col-md-6">
				<div class="inf_sobre">
					<h2 class="tit_sobre">Sobre a Engetermica</h2>
					<p class="desc_sobre">Constituída em julho de 1991, em Recife (PE), voltada inicialmente para a execução de serviços técnicos em geração e distribuição de vapor. Em 1993 foi inaugurada uma filial em Fortaleza com objetivo de comercializar materiais térmicos diretamente para a indústria, agregando valores e velocidade aos seus serviços com a aplicação de novas técnicas perfeitamente adaptadas as necessidades atuais do mercado, adicionando soluções criativas em função de cada projeto, através de metodologias e racionalização dos serviços para economia e preservação do meio ambiente.</p>

					<div id="missao">
					<strong class="subtit_sobre">Nossa missão</strong>
					<p class="desc_product">Fornecer soluções técnicas gerando segurança ambiental.</p>
					</div>

					<div id="visao">
					<strong class="subtit_sobre">Nossa Visão</strong>
					<p class="desc_sobre">Ser líder regional em soluções técnicas com inovação, lucratividade e desenvolvimento sustentável focado em parcerias contínuas.</p>
					</div>



				</div>
			</div>

			<div class="col-md-5 col-md-offset-1 sobre_img-box">
				<figure>
					<img src="img/fachada.png" alt="imagem representativa" class="img-responsive">
				</figure>
			</div>
		</div>
	</div>
</section>

<?php
	include("footer.php");
?>
