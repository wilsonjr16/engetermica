<?php
	$cabecalho_title = "Parceiros";
	include("header.php");
?>

<section class="section_parceiros fleft_full">
	<div class="center_element">
		<div class="section_parceiros-box">
			<div class="col-md-12">
				<div class="inf_parceiros">
					<h2 class="tit_parceiros">Parceiros</h2>

					<ul class="list_parceiros row">
						<li class="item_parceiro col-md-4">
							<img src="img/parceiros/ufc.png" class="img-responsive center-block">
							<h3 class="name_parceiro">Universidade Federal do Ceará</h3>
							<p class="desc_parceiro">Av. Humberto Monte - Bairro do Pici - Fortaleza/CE - CEP: 60.020-181 <strong class="fone_parceiro">Fone +55 (85) 3366-7300</strong></p>
							<a href="link_parceiro" title="Site do parceiro" class="link_parceiro">http://www.ufc.br</a>
						</li>

						<li class="item_parceiro col-md-4">
							<img src="img/parceiros/apegce.png" class="img-responsive center-block">
							<h3 class="name_parceiro">Associação das Empresas da Rede Produtiva do Petróleo, Energia e Gás do Ceará</h3>
							<p class="desc_parceiro">Av. Monsenhor Tabosa, 777 - Praia de Iracema - CEP: 60.150-010 <strong class="fone_parceiro">Fone: (85) 3255.6600</strong></p>
							<a href="link_parceiro" title="Site do parceiro" class="link_parceiro">http://www.apegce.com.br</a>
						</li>

						<li class="item_parceiro col-md-4">
							<img src="img/parceiros/sebrae.jpg" class="img-responsive center-block">
							<h3 class="name_parceiro">Serviço Brasileiro de Apoio às Micro e Pequenas Empresas – SEBRAE / CE</h3>
							<p class="desc_parceiro">Av. Monsenhor Tabosa, 777 - Praia de Iracema - Fortaleza/CE<strong class="fone_parceiro">Fone: (85) 3255.6600</strong></p>
							<a href="link_parceiro" title="Site do parceiro" class="link_parceiro">http://www.ce.sebrae.com.br</a>
						</li>

						<li class="item_parceiro col-md-4">
							<img src="img/parceiros/senai.jpg" class="img-responsive center-block">
							<h3 class="name_parceiro">Serviço Nacional de Aprendizagem Industrial – SENAI</h3>
							<p class="desc_parceiro">Av. Barão de Studart, 1980 - 1º andar - Aldeota- Fortaleza/CE<strong class="fone_parceiro">Tel: (85) 3466-5900</strong></p>
							<a href="link_parceiro" title="Site do parceiro" class="link_parceiro">http://www.senai-ce.org.br</a>
						</li>

						<li class="item_parceiro col-md-4">
							<img src="img/parceiros/ifce.jpg" class="img-responsive center-block">
							<h3 class="name_parceiro">Instituto Federal de Educação, Ciência e Tecnologia do Ceará</h3>
							<p class="desc_parceiro">Av. 13 de Maio, 2081 - Benfica - Fortaleza/CE  - CEP: 60.040-531 <strong class="fone_parceiro">Fone +55 (85) 3307-3666 Fax (85) 3307-3711</strong></p>
							<a href="link_parceiro" title="Site do parceiro" class="link_parceiro">http://ifce.edu.br/</a>
						</li>

						<li class="item_parceiro col-md-4">
							<img src="img/parceiros/redepetro.jpg" class="img-responsive center-block">
							<h3 class="name_parceiro">REDE PETRO - CE</h3>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	include("footer.php");
?>
