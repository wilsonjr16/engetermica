<footer class="footer fleft_full">
		<div class="center_element">
			<div class="institucional col-md-4">
				<h2 class="inst_tit">INSTITUCIONAL</h2>
				<ul class="inst_menu">
					<li class="inst_item">
						<a href="index.php" class="inst_link">Home</a>
					</li>

					<li class="inst_item">
						<a href="sobre.php" class="inst_link">Sobre</a>
					</li>

					<li class="inst_item">
						<a href="servicos.php" class="inst_link">Serviços</a>
					</li>

					<li class="inst_item">
						<a href="produtos.php" class="inst_link">Produtos</a>
					</li>

					<li class="inst_item">
						<a href="parceiros.php" class="inst_link">Parceiros</a>
					</li>

					<li class="inst_item">
						<a href="representadas.php" class="inst_link">Representadas</a>
					</li>

					<li class="inst_item">
						<a href="contato.php" class="inst_link">Contato</a>
					</li>
				</ul>
			</div>

			<div class="fale_conosco col-md-4">
				<h2 class="fale_tit">FALE CONOSCO</h2>
				<ul class="fale_menu">
					<li class="fale_item">
						<p class="fale_txt">Rua Raimundo Edmundo Linhares, 256 - Montese.</p>
						<p class="fale_txt">CEP: 60420-340 / Fortaleza - Ceará.</p>
					</li>

					<li class="fale_item">
						<p class="fale_txt">Fone: +55 85 3022-8080</p>
					</li>

					<li class="fale_item">
						<a class="fale_email-link" href="#">contato@engetermica.com.br</a>
					</li>
				</ul>
			</div>

			<div class="footer_media col-md-4">
				<h2 class="curta_page">CURTA NOSSA FANPAGE</h2>
				<div class="fb-page" data-href="https://www.facebook.com/engetermica/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/engetermica/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/engetermica/">Engetérmica</a></blockquote></div>
			</div>
		</div>
	</footer>

</body>

</html>
